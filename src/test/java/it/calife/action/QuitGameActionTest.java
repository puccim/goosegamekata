package it.calife.action;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

import it.calife.GooseGame;

public class QuitGameActionTest {
	
	static GooseGame game;
	static BaseAction command;
	
	@BeforeClass
	public static void setUp() {
		game= GooseGame.getIstance();
		command= new QuitGameAction();
	}
	
	@Test
	public void testQuitGameAction_ShouldReturnValidMessage() {
		String expected= QuitGameAction.QUIT_MESSAGE;
		
		String message= game.execute(command);
		
		assertEquals(expected,message);
	}
	
}
