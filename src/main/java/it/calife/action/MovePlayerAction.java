package it.calife.action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import it.calife.IGame;
import it.calife.model.PlayerPositionModel;

/**
 * Si occupa di implementare la logica per muovere un giocatore
 * @author mpucci
 * 
 * @TODO replace String+ with .format(String format, Object... args)   expression
 */
class MovePlayerAction implements BaseAction {
	
	private String player;
	private List<Integer> movement;

	public MovePlayerAction(String player, List<Integer> movement) {
		super();
		this.player = player;
		this.movement = movement;
	}

	public String getPlayer() {
		return player;
	}

	public void setPlayer(String player) {
		this.player = player;
	}

	public List<Integer> getMovement() {
		return movement;
	}

	public void setMovement(List<Integer> movement) {
		this.movement = movement;
	}

	public String run(IGame game) {

		Map<String, PlayerPositionModel> playersPositionMap = game.getGameModel().getPlayersPosition();
		
		String message= new String();
		
		Integer newPosition= null;
		
		Boolean playerExists= this.player!=null && playersPositionMap.get(this.player)!=null;
		
		if(playerExists) {
			
			PlayerPositionModel playerPosition= playersPositionMap.get(this.player);
			
			Integer currentPosition= playerPosition.getCurrentPosition(); // posizione del player prima di muovere
			
			Integer totalMove = movement.stream().reduce(0, Integer::sum); // num. celle da muovere
			
			String rollsStr = movement.stream().map(i->i.toString()).collect(Collectors.joining(", "));
			
			if(currentPosition+totalMove == game.getBoardModel().WIN_POSITION) /* player wins */ {
				
				newPosition=game.getBoardModel().WIN_POSITION;
				
				game.setRunning(false); // stop the game, player win
				
				message=this.player+" rolls "+rollsStr+". "+this.player+" moves from "+(currentPosition==0?"Start":currentPosition)+" to "+newPosition+". "+this.player+" Wins!!"; // . Pippo Wins!!
				
			} else if(currentPosition+totalMove > game.getBoardModel().WIN_POSITION) /* player bounces */ {
				
				// bounce formula
				newPosition= game.getBoardModel().WIN_POSITION - ( currentPosition + totalMove - game.getBoardModel().WIN_POSITION);
				
				message=this.player+" rolls "+rollsStr+". "+this.player+" moves from "+currentPosition+" to "+game.getBoardModel().WIN_POSITION+". "+this.player+" bounces! "+this.player+" returns to "+newPosition;
				
			} else /* normal player move */ {
				
				newPosition= currentPosition+totalMove;
				
				message=this.player+" rolls "+rollsStr+". "+this.player+" moves from "+(currentPosition==0?"Start":currentPosition)+" to "+newPosition;
				
			}
			
			// manage the on BRIDGE_POSITION event, go on AFTER_THE_BRIDGE_POSITION and overwrite message
			if(newPosition==game.getBoardModel().THE_BRIDGE_POSITION) {
				message= this.player+" rolls "+rollsStr+". "+this.player+" moves from "+currentPosition+" to The Bridge. "+this.player+" jumps to "+game.getBoardModel().AFTER_THE_BRIDGE_POSITION;
				newPosition= game.getBoardModel().AFTER_THE_BRIDGE_POSITION;
			}
			
			// manage the on THE GOSE event, multiple jump with two random dice and overwrite message
			if(Arrays.asList(game.getBoardModel().THE_GOOSE_POSITIONS).contains(newPosition)) {
				message=new String();
				message= this.player+" rolls "+rollsStr+". "+this.player+" moves from "+(currentPosition==0?"Start":currentPosition)+" to "+newPosition+", The Goose. ";
				while(Arrays.asList(game.getBoardModel().THE_GOOSE_POSITIONS).contains(newPosition)) {
					
					// rolls dice 2 times
					movement= new ArrayList<>();
					movement.add(GooseGameHelper.rollDice());
					movement.add(GooseGameHelper.rollDice());
					
					Integer newMove = movement.stream().reduce(0, Integer::sum); // num. celle da muovere
					
					newPosition+=newMove;

					message+=this.player+" moves again and goes to "+newPosition;
							
					if(Arrays.asList(game.getBoardModel().THE_GOOSE_POSITIONS).contains(newPosition))
						message+=", The Goose. ";
					
				}
				
			}
			
			playerPosition.setCurrentPosition(newPosition);
			playerPosition.setPreviousPosition(currentPosition);
			
			// final player position
			playersPositionMap.put(this.player, playerPosition);
			
			// Optional requirement: check other players position
			for(String s:playersPositionMap.keySet()) {
				PlayerPositionModel otherPlayerPosition= playersPositionMap.get(s);
				if(!s.equals(this.player) && otherPlayerPosition.getCurrentPosition()==playerPosition.getCurrentPosition()) {
					otherPlayerPosition.setCurrentPosition(otherPlayerPosition.getPreviousPosition());
					message+="  On "+newPosition+" there is "+s+", who returns to "+otherPlayerPosition.getPreviousPosition();
				}
			}
			
			
		} else /* Player not valid */ {
			
			 // TODO
			message= "Player "+this.player+" non valido";
			
		}
		
		return message;

	}

	@Override
	public String toString() {
		return "MovePlayerAction [player=" + player + ", movement=" + movement + "]";
	}
	
	
}