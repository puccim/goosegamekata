package it.calife;

import it.calife.action.BaseAction;
import it.calife.model.GooseGameBoardModel;
import it.calife.model.GooseGameStatusModel;

public interface IGame {

	public boolean isRunning();
	
	public void setRunning(boolean running);
	
	public GooseGameStatusModel getGameModel();
	
	public GooseGameBoardModel getBoardModel();

	public String getWelcomeMessage();

	public String getGameoverMessage();

	public String execute(BaseAction action);

}
