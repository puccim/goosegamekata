package it.calife.model;

public class PlayerPositionModel {

	private int previousPosition;
	private int currentPosition;
	
	public PlayerPositionModel() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public PlayerPositionModel(int previousPosition, int currentPosition) {
		super();
		this.previousPosition = previousPosition;
		this.currentPosition = currentPosition;
	}

	public int getPreviousPosition() {
		return previousPosition;
	}
	public void setPreviousPosition(int previousPosition) {
		this.previousPosition = previousPosition;
	}
	public int getCurrentPosition() {
		return currentPosition;
	}
	public void setCurrentPosition(int currentPosition) {
		this.currentPosition = currentPosition;
	}
	
	@Override
	public String toString() {
		return "PlayerPositionModel [previousPosition=" + previousPosition + ", currentPosition=" + currentPosition
				+ "]";
	}
	
}
