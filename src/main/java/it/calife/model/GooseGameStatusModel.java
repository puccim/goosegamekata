package it.calife.model;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author mpucci
 *
 */
public class GooseGameStatusModel {

	private Map<String,PlayerPositionModel> playersPosition; // "Pippo"=>12 , "Pluto"=>4

	public GooseGameStatusModel() {
		super();
		// TODO Auto-generated constructor stub
		this.playersPosition= new HashMap<String,PlayerPositionModel>();
	}

	public Map<String, PlayerPositionModel> getPlayersPosition() {
		return playersPosition;
	}

	public void setPlayersPosition(Map<String, PlayerPositionModel> playersPosition) {
		this.playersPosition = playersPosition;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((playersPosition == null) ? 0 : playersPosition.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GooseGameStatusModel other = (GooseGameStatusModel) obj;
		if (playersPosition == null) {
			if (other.playersPosition != null)
				return false;
		} else if (!playersPosition.equals(other.playersPosition))
			return false;
		return true;
	}
	
}
