package it.calife;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

import it.calife.action.ActionFactory;
import it.calife.action.BaseAction;

/**
 * Main class, si occupa di:
 * <ul>
 * <li> inizializzare e gestire la sessione di gioco </li>
 * <li> ricevere i comandi di gioco</li>
 * <li> presentare l' esito dei comandi impartiti </li>
 * </ul>
 * 
 * @author mpucci
 *
 */
public class GameConsole {
	
	public static final InputStream INPUT= System.in;
	public static final PrintStream OUTPUT= System.out;
	
	public static final String PROMPT="> ";
	
	protected static BaseAction read(InputStream input) {
		return ActionFactory.parseAction(new Scanner(INPUT)); // static factory method
	}
	
	protected static void println(PrintStream output,String message) {
		output.println(message);
	}
	
	protected static void print(PrintStream output,String message) {
		output.print(message);
	}
	
	// Repl method
	public static void main(String...args) {
		
		GooseGame game= GooseGame.getIstance(); // singleton
		
		print(OUTPUT,game.getWelcomeMessage());
		
		while(game.isRunning()) /* loop */ {
			
			print(OUTPUT,PROMPT);
			
			// read
			BaseAction command= read(INPUT);

			// eval
			String message= game.execute(command);
			
			// print
			println(OUTPUT,message);
			
		}
		
		print(OUTPUT,game.getGameoverMessage());
		
	}
	
}
