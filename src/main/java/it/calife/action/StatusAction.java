package it.calife.action;

import java.util.Map;

import it.calife.IGame;
import it.calife.model.PlayerPositionModel;

class StatusAction implements BaseAction {

    public StatusAction() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String run(IGame game) {
        
    	String message= new String();
    	
    	Map<String, PlayerPositionModel> playersPositionMap = game.getGameModel().getPlayersPosition();
    	
    	if(playersPositionMap.size()>0) {
    		for(String s:playersPositionMap.keySet()) {
    			PlayerPositionModel pp= playersPositionMap.get(s);
    			message+="Player: "+s+" , current: "+pp.getCurrentPosition()+" , previous: "+pp.getPreviousPosition()+System.lineSeparator();
    		}	
    	} else  {
    		message= "No players defined. No position to show.";
    	}
    	 
        return message;
    }

	@Override
	public String toString() {
		return "StatusAction []";
	}
	
}



