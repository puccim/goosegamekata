package it.calife.action;

import static org.junit.Assert.assertTrue;

import java.util.Scanner;

import org.junit.Test;

public class ActionFactoryTest {
	
	@Test
	public void testParseAction_HelpShouldReturnHelpAction() {
		String source= "help";
		BaseAction result= ActionFactory.parseAction(new Scanner(source));
		assertTrue(result instanceof HelpAction);
	}

	@Test
	public void testParseAction_QuitShouldReturnQuitAction() {
		String source= "quit";
		BaseAction result= ActionFactory.parseAction(new Scanner(source));
		assertTrue(result instanceof QuitGameAction);
	}
	
	@Test
	public void testParseAction_ExitShouldReturnQuitAction() {
		String source= "exit";
		BaseAction result= ActionFactory.parseAction(new Scanner(source));
		assertTrue(result instanceof QuitGameAction);
	}
	
	@Test
	public void testParseAction_StatusShouldReturnQuitAction() {
		String source= "status";
		BaseAction result= ActionFactory.parseAction(new Scanner(source));
		assertTrue(result instanceof StatusAction);
	}
	
	@Test
	public void testParseAction_AddPlayerShouldReturnAddPlayerAction() {
		String source= "add player one";
		BaseAction result= ActionFactory.parseAction(new Scanner(source));
		assertTrue(result instanceof AddPlayerAction);
	}
	
	@Test
	public void testParseAction_AddPlayerWithNoArgumentShouldReturnHelpAction() {
		String source= "add player";
		BaseAction result= ActionFactory.parseAction(new Scanner(source));
		assertTrue(result instanceof HelpAction);
	}
	
	@Test
	public void testParseAction_MovePlayerShouldReturnMovePlayerAction() {
		String source= "move one";
		BaseAction result= ActionFactory.parseAction(new Scanner(source));
		assertTrue(result instanceof MovePlayerAction);
	}
	
	@Test
	public void testParseAction_MovePlayerWithNoPlayerShouldReturnHelpAction() {
		String source= "move";
		BaseAction result= ActionFactory.parseAction(new Scanner(source));
		assertTrue(result instanceof HelpAction);
	}
	
	@Test
	public void testParseAction_MovePlayerWithArgumentsShouldReturnMovePlayerAction() {
		String source= "move one 1, 2";
		BaseAction result= ActionFactory.parseAction(new Scanner(source));
		assertTrue(result instanceof MovePlayerAction);
		assertTrue(((MovePlayerAction)result).getPlayer().equals("one"));
	}
	
	@Test
	public void testParseAction_MovePlayerWithArgumentsShouldReturnValidParameters() {
		String source= "move one 1, 2";
		BaseAction result= ActionFactory.parseAction(new Scanner(source));
		
		assertTrue(1==((MovePlayerAction)result).getMovement().get(0));
		assertTrue(2==((MovePlayerAction)result).getMovement().get(1));
		
	}
	
	// GAME THROW THE DICE
	@Test
	public void testParseAction_MovePlayerWithNoArgumentsShouldReturnValidParameters() {
		String source= "move one";
		BaseAction result= ActionFactory.parseAction(new Scanner(source));
		
		assertTrue(0!=((MovePlayerAction)result).getMovement().get(0));
		assertTrue(0!=((MovePlayerAction)result).getMovement().get(1));
		
	}
	
	@Test
	public void testParseAction_MovePlayerWithNotParseableArgumentsShouldReturnValidParameters() {
		String source= "move one sahjs, sahsaj";
		BaseAction result= ActionFactory.parseAction(new Scanner(source));
		
		assertTrue(0!=((MovePlayerAction)result).getMovement().get(0));
		assertTrue(0!=((MovePlayerAction)result).getMovement().get(1));
		
	}
	
}
