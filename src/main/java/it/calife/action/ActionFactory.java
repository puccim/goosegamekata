package it.calife.action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Si occupa di istanziare la action relativa al comando ricevuto in input
 * Se il comando ricevuto non e' un comando valido viene restituita la HelpAction
 * @author mpucci
 *
 */
public class ActionFactory {
	
	public static BaseAction parseAction(Scanner scanner) {
		
		BaseAction result= null;
		
		if(scanner.hasNextLine()) {
			
			String line= scanner.nextLine();
			
			List<String> command= Arrays.asList(line.split(" "));

			if(command!=null && command.size()>0 && command.get(0).length()>0) {
				String op =command.get(0);
				
				switch (op) {
		         case "add":
		        	 if(command.size()==3 && command.get(1).equals("player")) {
		        		 String player= command.get(2);
		        		 result= new AddPlayerAction(player);
		        	 }
		        	 break;
		         case "move":
		        	 if(command.size()>=2) {
		        		 String player= command.get(1);
		        		 
		        		 List<Integer> movements= new ArrayList<>(2);
		        		 if(command.size()>=4) {
		        			 movements.add(isNumeric(command.get(2).replace(",","").trim())? Integer.parseInt(command.get(2).replace(",","").trim()) : GooseGameHelper.rollDice() );
		        			 movements.add(isNumeric(command.get(3).replace(",","").trim())? Integer.parseInt(command.get(3).replace(",","").trim()) : GooseGameHelper.rollDice() );
		        		 } else {
		        			 movements.add(GooseGameHelper.rollDice());
		        			 movements.add(GooseGameHelper.rollDice());
		        		 }
		        		 
		        		 result= new MovePlayerAction(player,movements);
		        	 }
		        	 break;
		         case "status":
		        	 result= new StatusAction();
		        	 break;
		         case "help":
		        	 result= new HelpAction();
		        	 break;
		         case "exit":
		         case "quit":
		        	 result= new QuitGameAction();
		        	 break;
		         default:
		        	 result= new HelpAction();
		        	 break;
				}
				
			}
				
		}
		
		// default to help
		if(result!=null)
			return result;
		else
			return new HelpAction();
		
	}
	
	private static boolean isNumeric(String strNum) {
	    if (strNum == null) {
	        return false;
	    }
	    try {
	        double d = Integer.parseInt(strNum);
	    } catch (NumberFormatException nfe) {
	        return false;
	    }
	    return true;
	}
	
}
