# Goose game

Implementation of the goose game in Java, based on requirements from [xpeppers/goose-game-kata](https://github.com/xpeppers/goose-game-kata).

## Usage

Run the GameConsole file to see the result.

```bash
mvn clean compile exec:java
```

## Test

```bash
mvn test
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
