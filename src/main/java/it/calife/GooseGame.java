package it.calife;

import it.calife.action.BaseAction;
import it.calife.model.GooseGameBoardModel;
import it.calife.model.GooseGameStatusModel;

/**
 * 
 * @author mpucci
 *
 */
public class GooseGame implements IGame {

	private static GooseGame istance=null;

	private boolean running;
	
	private GooseGameStatusModel gameModel;
	private GooseGameBoardModel boardModel;
	
	private final String WELCOME_MESSAGE="################# WELCOME TO GOOSE GAME #################"+System.lineSeparator()+"Type a command below:";
	private final String GAMEOVER_MESSAGE= "################# GAME OVER #################";
	
	private GooseGame() {
		super();
		
		this.running= true;
		this.gameModel= new GooseGameStatusModel();
		this.boardModel= new GooseGameBoardModel();
	}

    public static GooseGame getIstance() {
	    if(istance==null)
	      istance = new GooseGame();
	    return istance;
	  }

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}
	
	public GooseGameStatusModel getGameModel() {
		return gameModel;
	}

	public void setGameModel(GooseGameStatusModel gameModel) {
		this.gameModel = gameModel;
	}

	public GooseGameBoardModel getBoardModel() {
		return boardModel;
	}

	public void setBoardModel(GooseGameBoardModel boardModel) {
		this.boardModel = boardModel;
	}

	public String getWelcomeMessage() {
		return WELCOME_MESSAGE;
	}

	public String getGameoverMessage() {
		return GAMEOVER_MESSAGE;
	}

	public String execute(BaseAction action) {
		return action.run(this);
	}

}
