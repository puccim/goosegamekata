package it.calife.action;

import it.calife.IGame;

class HelpAction implements BaseAction {
	
	public static final String HELP_MESSAGE=""+System.lineSeparator()+
									        "|  Commands:"+System.lineSeparator()+
									        "|    help                            Provides help for a given command"+System.lineSeparator()+
									        "|    exit                            Exits application."+System.lineSeparator()+
									        "|    add player <name>               Add new player"+System.lineSeparator()+
									        "|    move <player> [roll1], [roll2]  Make a move of a player"+System.lineSeparator()+
									        "|    status                          Show player position."+System.lineSeparator()+
									        "|"+System.lineSeparator();

    public HelpAction() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String run(IGame game) {
        return HELP_MESSAGE;
    }

	@Override
	public String toString() {
		return "HelpAction []";
	}
    
}



