package it.calife.action;

import it.calife.IGame;

class QuitGameAction implements BaseAction {
	
	public static final String QUIT_MESSAGE= "Quit the game ";

    public QuitGameAction() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String run(IGame game) {
        game.setRunning(false);
        return QUIT_MESSAGE;
    }

	@Override
	public String toString() {
		return "QuitGameAction []";
	}
	
}



