package it.calife.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import it.calife.GooseGame;
import it.calife.model.GooseGameBoardModel;
import it.calife.model.GooseGameStatusModel;

/*
 * 
 * 	If there are two participants "Pippo" and "Pluto" on space "Start"
	the user writes: "move Pippo 4, 2"
	the system responds: "Pippo rolls 4, 2. Pippo moves from Start to 6"
	the user writes: "move Pluto 2, 2"
	the system responds: "Pluto rolls 2, 2. Pluto moves from Start to 4"
	the user writes: "move Pippo 2, 3"
	the system responds: "Pippo rolls 2, 3. Pippo moves from 6 to 11"
 */
public class MovePlayerActionTest {
	
	static GooseGame game;
	
	@Before
	public void setUp() {
		game= GooseGame.getIstance();
		
		// reset singleton
		game.setRunning(true);
		game.setBoardModel(new GooseGameBoardModel());
		game.setGameModel(new GooseGameStatusModel());
	}
	
	// WIN
	@Test
	public void testMovePlayerAction_ShouldReturnWinMessage_WhenLandOnCell63() {
		
		String expected= "Pippo rolls 1, 2. Pippo moves from 60 to 63. Pippo Wins!!";
		
		this.game.execute(new AddPlayerAction("Pippo"));
		
		BaseAction command= new MovePlayerAction("Pippo",Arrays.asList(new Integer[] {58,2})); // Pippo on cell 60
		this.game.execute(command);
		
		command= new MovePlayerAction("Pippo",Arrays.asList(new Integer[] {1,2})); // Pippo on cell 63 now
		String message= MovePlayerActionTest.game.execute(command);
		
		assertEquals(expected,message);
	}
	
	// THE BRIDGE
	@Test
	public void testMovePlayerAction_ShouldReturnValidMessage_WhenLandOnCellTheBridge() {
		
		String expected= "Pippo rolls 1, 1. Pippo moves from 4 to The Bridge. Pippo jumps to 12";
		
		this.game.execute(new AddPlayerAction("Pippo"));
		
		BaseAction command= new MovePlayerAction("Pippo",Arrays.asList(new Integer[] {1,3})); // Pippo on cell 4
		this.game.execute(command);
		
		command= new MovePlayerAction("Pippo",Arrays.asList(new Integer[] {1,1})); // Pippo on cell 6 now
		String message= this.game.execute(command);
		
		assertEquals(expected,message);
	}
	
	// THE GOOSE
	@Test
	public void testMovePlayerAction_ShouldReturnValidMessage_WhenLandOnCelllTheGoose() {
		
		String expected= "Pippo rolls 1, 1. Pippo moves from 3 to 5, The Goose. Pippo moves again and goes to ";
		
		this.game.execute(new AddPlayerAction("Pippo"));
		
		BaseAction command= new MovePlayerAction("Pippo",Arrays.asList(new Integer[] {1,2})); // Pippo on cell 3
		this.game.execute(command);
		
		command= new MovePlayerAction("Pippo",Arrays.asList(new Integer[] {1,1})); // Pippo on cell 5 now, The Goose
		String message= this.game.execute(command);
		
		assertTrue(message.contains(expected));
		
	}
	
}
