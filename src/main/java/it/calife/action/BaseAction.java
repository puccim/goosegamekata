package it.calife.action;

import it.calife.IGame;

public interface BaseAction {

	public String run(IGame game);
	
}
