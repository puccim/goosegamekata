package it.calife.action;

import java.util.Map;
import java.util.Set;

import it.calife.IGame;
import it.calife.model.PlayerPositionModel;

/**
 * Gestisce la logica per aggiungere nuovi player al GooseGame
 * @author mpucci
 *
 */
class AddPlayerAction implements BaseAction {

	private String player;

	public AddPlayerAction(String player) {
		super();
		this.player = player;
	}

	public String getPlayer() {
		return player;
	}

	public void setPlayer(String player) {
		this.player = player;
	}

	public String run(IGame game) {

		Map<String, PlayerPositionModel> playersPositionMap = game.getGameModel().getPlayersPosition();
		
		String message= new String();

		if (playersPositionMap.containsKey(this.player)) /* player already exists */ {
		
			message= this.player+": already existing player";
		
		} else /* put player on start position*/ {
			
			PlayerPositionModel playerPosition= new PlayerPositionModel(game.getBoardModel().START_POSITION,game.getBoardModel().START_POSITION); // set initial position (both current and previous)
			
			playersPositionMap.put(player, playerPosition);

			Set<String> p = playersPositionMap.keySet();

			String playersStr = String.join(", ", p); // Pippo, Pluto

			message= "players: " + playersStr;
		}
		
		return message;

	}

	@Override
	public String toString() {
		return "AddPlayerAction [player=" + player + "]";
	}
	
}
