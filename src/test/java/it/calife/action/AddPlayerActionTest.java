package it.calife.action;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import it.calife.GooseGame;
import it.calife.model.GooseGameBoardModel;
import it.calife.model.GooseGameStatusModel;

/**
 * Si occupa di implementare la logica per aggiungere un giocatore
 * @author mpucci
 *
 */
public class AddPlayerActionTest {
	
	static GooseGame game;
	
	@Before
	public void setUp() {
		game= GooseGame.getIstance();
		
		// reset singleton
		game.setRunning(true);
		game.setBoardModel(new GooseGameBoardModel());
		game.setGameModel(new GooseGameStatusModel());
		
	}
	
	@Test
	public void testAddPlayerAction_ShouldReturnValidMessage() {
		String expected= "players: Pippo";
		
		BaseAction command= new AddPlayerAction("Pippo");

		GooseGame game= GooseGame.getIstance();
		String message= game.execute(command);
		
		assertEquals(expected,message);
	}
	
	@Test
	public void testAddPlayerAction_ShouldReturnValidMessageWhenTwoPlayerExists() {
		String expected= "players: Pippo, Pluto";
		
		GooseGame game= GooseGame.getIstance();
		game.execute(new AddPlayerAction("Pippo"));
		
		String message= game.execute(new AddPlayerAction("Pluto"));
		
		assertEquals(expected,message);
	}
	
	@Test
	public void testAddPlayerAction_ShouldReturnDuplicateMessage() {
		String expected= "Pippo: already existing player";
		
		GooseGame game= GooseGame.getIstance();
		game.execute(new AddPlayerAction("Pippo"));
		
		String message= game.execute(new AddPlayerAction("Pippo"));
		
		assertEquals(expected,message);
	}
	
}
