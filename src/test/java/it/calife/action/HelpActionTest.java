package it.calife.action;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

import it.calife.GooseGame;

public class HelpActionTest {
	
	static GooseGame game;
	static BaseAction command;
	
	@BeforeClass
	public static void setUp() {
		game= GooseGame.getIstance();
		command= new HelpAction();
	}
	
	@Test
	public void testHelpAction_ShouldReturnValidMessage() {
		String expected= HelpAction.HELP_MESSAGE;
		
		String message= game.execute(command);
		
		assertEquals(expected,message);
	}
	
}
